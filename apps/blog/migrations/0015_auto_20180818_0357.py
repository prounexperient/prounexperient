# Generated by Django 2.0.2 on 2018-08-18 06:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0014_auto_20180310_1748'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blogpostsmodel',
            name='t_height',
            field=models.IntegerField(default=0, null=0),
        ),
        migrations.AlterField(
            model_name='blogpostsmodel',
            name='t_width',
            field=models.IntegerField(default=0, null=0),
        ),
        migrations.AlterField(
            model_name='blogpostsmodel',
            name='thumbnail',
            field=models.ImageField(blank=True, default='blog/default.png', height_field='t_height', null=True, upload_to='blog/thumbnail', width_field='t_width'),
        ),
    ]
