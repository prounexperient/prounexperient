# Generated by Django 2.0.2 on 2018-02-21 07:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_imagemodel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='imagemodel',
            name='image',
            field=models.ImageField(default='media/blog/default.png', height_field='height', upload_to='media/blog', width_field='width'),
        ),
    ]
