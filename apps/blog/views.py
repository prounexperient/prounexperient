from django.views.generic.list import MultipleObjectMixin
from django.views.generic.base import View
from django.views.generic.edit import ModelFormMixin
from django.views.generic import DetailView, UpdateView

from django.views.decorators.cache import cache_page
from django.utils.decorators import method_decorator

from django.http import Http404
from django.shortcuts import render

from apps.image.forms import ImageForm
from apps.image.models import ImageModel

from .models import Blog
from .forms import CreatePostForm
from apps.analytics import analytics


object_name = 'blog_posts'


# @method_decorator(cache_page(60 * 480 * 2, key_prefix='BlogHomeView'), name='dispatch') #cache for 8 hours
class BlogHomeView(View, MultipleObjectMixin, ModelFormMixin):

    form_class = CreatePostForm     # FormMixin attr
    queryset = Blog.objects.all()   # MultipleObjectMixin attr, SingleObjectMixin attr
    paginate_by = 3                 # MultipleObjectMixin attr
    object = None                   # ModelFormMixin get_success_url

    def get(self, request, *args, **kwargs):
        analytics.pageAnalytics(request)
        context = self.get_context_data(**kwargs)
        return render(request, 'blog/blog.html', context)

    def post(self, request, *args, **kwargs):
        form = self.get_form()

        if not form.is_valid():
            return self.form_invalid(form)

        return self.form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(object_list=self.queryset, **kwargs)
        context['create_post_form'] =  self.get_form()
        return context

    def form_valid(self, form):
        form.instance.publisher = self.request.user
        return super(BlogHomeView, self).form_valid(form)


# @method_decorator(cache_page(60 * 60 * 100), name='dispatch')
class PostDetailView(DetailView, ModelFormMixin):
    template_name = 'blog/post.html'
    model = Blog                    # SingleObjectMixin
    form_class = CreatePostForm     # ModelFormMixin
    object = Blog()

    def post(self, request, *args, **kwargs):
        instance = Blog.objects.get(slug=kwargs['slug'])
        form = CreatePostForm(request.POST or None, files=request.FILES, instance=instance)

        if not form.is_valid():
            return self.form_invalid(form)

        return self.form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        form.instance.publisher = self.request.user      
        return super(PostDetailView, self).form_valid(form)


class PostUpdateView(UpdateView):
    model = Blog
    form_class = CreatePostForm
    template_name_suffix = '_update_form'

# TODO: Change or add banner and/or extra images using the
# ImageModel or ImageForm via ajax