from django.db import models
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.utils.deconstruct import deconstructible
from django.conf import settings

import os
from uuid import uuid4


@deconstructible
class PathAndRename(object):

    def __init__(self, sub_path):
        self.path = sub_path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join(self.path, filename)


path_and_rename = PathAndRename("blog/thumbnail")


class Blog(models.Model):
    publisher   = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    title       = models.CharField(max_length=128)
    subtitle    = models.CharField(max_length=256, null=True, blank=True)
    content     = models.TextField()
    topic       = models.CharField(max_length=128, default='Not Categorized', null=True, blank=True)
    updated     = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp   = models.DateTimeField(auto_now=False, auto_now_add=True)
    slug        = models.SlugField(max_length=256, null=True, blank=True)
    t_width     = models.IntegerField(default=0, null=True, blank=True)
    t_height    = models.IntegerField(default=0, null=True, blank=True)
    thumbnail   = models.ImageField(
        upload_to=path_and_rename, 
        width_field='t_width', 
        height_field='t_height', 
        default='blog/default.png',
        null=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post:post-detail', kwargs={'slug': self.slug})

    def related_posts(self):
        return Blog.objects.filter(topic=self.topic)[:3]

    class Meta:
        ordering = ['-timestamp', '-updated']

