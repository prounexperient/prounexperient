from django.contrib import admin

from .models import Blog
from apps.image.models import ImageModel


class ImageInline(admin.TabularInline):
    model = ImageModel


class BlogPostsAdmin(admin.ModelAdmin):
    inlines = [
        ImageInline,
    ]


admin.site.register(Blog, BlogPostsAdmin)
