from django.forms import ModelForm, TextInput, Textarea
from .models import Blog


class CreatePostForm(ModelForm):
    class Meta:
        model = Blog
        fields = ['topic', 'title', 'subtitle', 'content', 'thumbnail']

        widgets = {
            'topic': TextInput({'class': 'form-control'}),
            'title': TextInput(attrs={'class': 'form-control'}),
            'subtitle': TextInput({'class': 'form-control'}),
            'content': Textarea(attrs={'class': 'form-control', 'rows': 4, 'placeholder': 'Enter your message'}),
        }