from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
import os

from .models import Blog

from utils import utils


@receiver(pre_save, sender=Blog)
def slug(sender, **kwargs):
    '''
    create or update the slug on pre saving the instance
    We'll be creating the slug based on title attribute of instance
    '''
    slug = kwargs["instance"].title
    
    for c in utils.pattern:
        if not slug.count(c):
            continue
        slug = slug.replace(c, '')

    slug = '-'.join(slug.split(' ')).lower()
    kwargs["instance"].slug = utils.translate(slug)


@receiver(post_save, sender=Blog)
def rename_image(sender, **kwargs):
    pass
