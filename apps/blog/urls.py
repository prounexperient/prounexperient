from django.urls import path
from django.views.decorators.cache import cache_page

from apps.blog.views import BlogHomeView, PostDetailView, PostUpdateView

app_name = 'blog'

urlpatterns = [
    # path('blog/', cache_page(60 * 480 * 2, key_prefix='BlogHomeView')(BlogHomeView.as_view()), name='blog'),
    path('blog/', BlogHomeView.as_view(), name='blog'),
    path('blog/home/', BlogHomeView.as_view(), name='blog'),
    path('blog/post/<int:pk>/', PostDetailView.as_view(), name='post-detail'),
    path('blog/post/<slug:slug>/', PostDetailView.as_view(), name='post-detail'),
    path('blog/post/<int:pk>/update/', PostUpdateView.as_view(), name='post-update'),
    path('blog/post/<slug:slug>/update/', PostUpdateView.as_view(), name='post-update'),
]