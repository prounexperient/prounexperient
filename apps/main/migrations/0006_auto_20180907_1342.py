# Generated by Django 2.0.2 on 2018-09-07 16:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0005_auto_20180907_1329'),
    ]

    operations = [
        migrations.RenameField(
            model_name='webpageactivity',
            old_name='first_visit',
            new_name='first_access',
        ),
        migrations.RenameField(
            model_name='webpageactivity',
            old_name='last_visit',
            new_name='last_access',
        ),
    ]
