from django.db import models


class ContactModel(models.Model):
    name        = models.CharField(max_length=128)
    email       = models.EmailField(max_length=128, default='')
    subject     = models.CharField(max_length=128)
    content     = models.TextField()
    timestamp   = models.DateTimeField(auto_now=False, auto_now_add=True)

    class Meta:
        verbose_name_plural = 'Contacts'

    def __str__(self):
        return self.name

