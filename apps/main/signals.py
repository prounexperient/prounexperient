from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.core.mail import send_mail
from django.conf import settings
from .models import ContactModel

from smtplib import SMTP
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


@receiver(post_save, sender=ContactModel)
def notify_admin(sender, **kwargs):
    instance = kwargs["instance"]
    messageBody = """
        Subject: {}\n\nUser name: {}\n\nMessage: {}\nSent at: {}
    """.format (
        instance.subject,
        instance.name,
        instance.content,
        instance.timestamp
    )
    HTMLMessage = """
        <h2>{}</h2>
        <p style="font-size:1.3em;color:blue;font-weight:400;">{}</p>
        <p>{}</p>
        <p style="font-style:italic;">{}</p>
    """.format(
        instance.name,
        instance.subject,
        instance.content,
        instance.timestamp
    )
    send_mail (
        'Contact has been sent from Prounexperient', 
        messageBody, 
        'rod.contato@hotmail.com', 
        [settings.EMAIL_HOST_USER],
        html_message=HTMLMessage,
        fail_silently=True
    )
    
###################################################

@receiver(post_save, sender=ContactModel)
def email_validation(sender, **kwargs):
    pass