from django.contrib import admin

from .models import ContactModel

class ContactModelAdmin(admin.ModelAdmin):
    list_display    = ['name', 'timestamp']
    list_filter     = ['timestamp']
    search_fields   = ['name', 'subject']
    
    class Meta:
        model = ContactModel


admin.site.register(ContactModel, ContactModelAdmin)

