from django.urls import path
from django.shortcuts import redirect
from django.views.generic import TemplateView, RedirectView

from apps.main.views import HomeView, UnderDevelopmentionView

app_name = 'main'

urlpatterns = [
    path('', RedirectView.as_view(url='/under-development/'), name='home'),
    # path('', HomeView.as_view(), name='home'),
    path('under-development/', UnderDevelopmentionView.as_view(), name='under_development'),
    path('contact-success/',
        TemplateView.as_view(template_name='main/contact_success.html'), 
        name='contact-success'),
]
