from django.forms import ModelForm, Textarea, EmailInput, TextInput
from .models import ContactModel


class ContactForm(ModelForm):
    class Meta:
        model = ContactModel
        fields = ['name', 'email', 'subject', 'content']

        labels = {
            'name': 'Name ',
            'email': 'Email address ',
            'subject': 'Subject ',
            'content': 'Message ',
        }

        widgets = {
            'name': TextInput(attrs={'placeholder': 'company name'}),
            'email': EmailInput(attrs={'placeholder': 'email@example.com'}),
            'subject': TextInput({'placeholder': 'subject'}),
            'content': Textarea(attrs={'rows': 4, 'placeholder': 'message'}),
        }

