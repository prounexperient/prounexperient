from django.shortcuts import render
from django.views import View
from django.views.generic import TemplateView, CreateView

from .forms import ContactForm
from apps.analytics import analytics


class UnderDevelopmentionView(CreateView):
    template_name = 'under_development.html'
    success_url = '/contact-success/'
    form_class = ContactForm

    def get(self, request, *args, **kwargs):
        analytics.pageAnalytics(request)
        return super().get(request, *args, **kwargs)


class HomeView(CreateView):
    template_name = 'main/base.html'
    success_url = '/contact-success/'
    form_class = ContactForm

    def get(self, request, *args, **kwargs):
        analytics.pageAnalytics(request)
        return super().get(request, *args, **kwargs)

