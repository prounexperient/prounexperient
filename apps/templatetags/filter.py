from django import template

register = template.Library()

"""
    Custom template tags to add CSS formatting to the django Form in the template
"""

@register.filter(name='addclass')
def addclass(value, argument):
    return value.as_widget(attrs={'class': argument})

@register.filter(name='addfor')
def addfor(value, argument):
    return value.as_widget(attrs={'for': argument})

@register.filter(name='addid')
def addid(value, argument):
    return value.as_widget(attrs={'id': argument})

@register.filter(name='addplaceholder')
def addplaceholder(value, argument):
    return value.as_widget(attrs={'placeholder': argument})


@register.filter(name='disqusCountComments')
def disqusCountComments(value, argument):
    return value.rstrip('/') + argument