from django.db import models


class WebpageActivity(models.Model):
    ip = models.CharField(max_length=32)
    url = models.CharField(max_length=256, default='prounexperient.herokuapp.com')
    browser = models.CharField(max_length=256, default='')
    first_access = models.DateTimeField(auto_now=False, auto_now_add=True)
    last_access = models.DateTimeField(auto_now=True, auto_now_add=False)


    class Meta:
        verbose_name_plural = 'Webpage Activities'
        ordering = ['-last_access', '-first_access']

    def __str__(self):
        return '{} last access at {} on the {}'.format(
            self.ip, self.last_access, self.url
        )
