from django.contrib import admin
from .models import WebpageActivity


class WebpageActivityAdmin(admin.ModelAdmin):
    fields = [('ip', 'first_access', 'last_access'), 'url', 'browser' ]
    readonly_fields = ['first_access', 'last_access']

    # fieldsets = (
    #     (None, {
    #         'fields': ['ip']
    #     }),
    #      ('Access Date Info', {
    #         'classes': ('collapse',),
    #         'fields': ['first_access', 'last_access'],
    #         # 'readonly_fields': ['first_access', 'last_access'],
    #     })
    # )
   
    class Meta:
        model = WebpageActivity


admin.site.register(WebpageActivity, WebpageActivityAdmin)