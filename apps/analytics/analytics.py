from .models import WebpageActivity

from datetime import datetime
import pytz


def pageAnalytics(r):
    '''
    @param r : request
    '''
    x_forwarded_for = r.META.get('HTTP_X_FORWARDED_FOR')
    address = r.META.get('HTTP_HOST') + r.META.get('PATH_INFO')
    browser = r.META.get('HTTP_USER_AGENT')
    registered = False
    sptz = pytz.timezone('America/Sao_Paulo')
    
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = r.META.get('REMOTE_ADDR')
    
    registered = WebpageActivity.objects.filter(ip=ip)
    
    if not registered:
        WebpageActivity.objects.create(
            ip=ip,
            url=address,
            browser=browser,
            first_access=sptz.localize(datetime.now()),
            last_access=sptz.localize(datetime.now())).save()
    else:
        update = WebpageActivity.objects.get(ip=ip)
        update.url = address
        update.last_access = sptz.localize(datetime.now())
        update.save()