from django.contrib import admin
from .models import ImageModel


class ImageModelAdmin(admin.ModelAdmin):
    model = ImageModel


admin.site.register(ImageModel)
