from django.db import models

from apps.blog.models import Blog


class ImageModel(models.Model):
    blog        = models.ForeignKey(Blog, on_delete=models.CASCADE, related_name='images')
    filename    = models.CharField(max_length=126)
    width       = models.IntegerField(default=0)
    height      = models.IntegerField(default=0)
    image       = models.ImageField(upload_to='blog/thumbnail', width_field='width', height_field='height', default='blog/default.png')

    class Meta:
        verbose_name_plural = 'Images'

    def __str__(self):
        return self.filename


