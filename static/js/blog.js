$(document).on('focusin', function(e) {
	// prevent Bootstrap from hijacking TinyMCE modal focus  
	if ($(e.target).closest(".mce-window").length) {
        e.stopImmediatePropagation();
    }
});