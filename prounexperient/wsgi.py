"""
WSGI config for prounexperient project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

try:
	from whitenoise.django import DjangoWhiteNoise
	wnd_active = True
except ImportError:
	wnd_active = False

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "prounexperient.settings")

application = get_wsgi_application()

if wnd_active:
	application = DjangoWhiteNoise(application)
