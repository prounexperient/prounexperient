# characters not allowed for a url
pattern = '[\!@#$%^&*()+=\'",;:\/\`~]?.'

# special portuguese letters
ptl = ['é', 'í', 'ó', 'ú', 'á', 'à', 'ã', 'õ', 'â', 'ô', 'ê', 'ç']
ptd = {'é':'e', 'í':'i', 'ó':'o', 'á':'a', 'ú':'u', 'à':'a', 'ã':'a', 'õ':'o', 'â':'a', 'ê':'e', 'ô':'o', 'ç':'c'}

def translate(t):
    '''
    arg: t => str
    return: t => str
    '''
    for l in t:
        if l in ptl:
            t = t.replace(l, ptd[ptl[ptl.index(l)]])
    return t